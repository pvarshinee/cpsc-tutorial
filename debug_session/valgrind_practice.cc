
/*
 * This program has various memory related problems that provide a good way
 * to show off the various abilities of valgrind.  To run it:
 *
 *   valgrind <optional valgrind options> ./valgrind-tests <test number>
 *
 * where <test number> is between 1 and 9, inclusive.  Suggested
 * valgrind options to run with are
 *
 *   --log-file=valgrind.output --num-callers=6 --leak-check=yes
 */

//#include <cassert>
//#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void
test_1()
{
  int i;
  printf("%d\n", i);               
  int * num = (int*)malloc(sizeof(int));
  printf("\n%d",*num);            
  free(num);
}

void
test_2()
{
  int *i = (int*)malloc(sizeof(int));
  free(i);
  *i = 4;                           
}

void
test_3()
{
  int * i = (int*)malloc(sizeof(int)*10);
  i[10] = 13;                     
  printf("\n%d", i[-1] );         
  free(i);
}

void
test_4()
{
  int i;
  int * ptr = &i;
  ptr[-8] = 7;                     
  i = ptr[-15];                    
}

void
test_5()
{
  int    * i = (int*) malloc ( sizeof(int) );
  double * j = (double*) malloc( sizeof( double) );
  i = NULL;
}

void
test_7()
{
  char big_buf[1000];
  char * ptr_1 = &big_buf[0];
  char * ptr_2 = &big_buf[400];
  memcpy(ptr_1, ptr_2, 500);       
}

void
test_8()
{
  int    * i = (int*) malloc ( sizeof(int) );
  free(i);
  free(i);
}

void
test_9()
{
  char * buf = (char*) malloc ( sizeof(char) * 50 );
  printf("Please type a bunch of characters and hit enter.\n");
  read(0, buf, 1000);              
  write(1, buf, 1000);             
  free(buf);
}

int
main(int argc, char**argv)
{
  if (argc!=2) {
    printf("Syntax:");
    printf(" %s <test-number>\n", argv[0] );
    return -1;
  }
  int test_number = atoi(argv[1]);

  switch (test_number) {
    case 1: test_1(); break;
    case 2: test_2(); break;
    case 3: test_3(); break;
    case 4: test_4(); break;
    case 5: test_5(); break;
    case 7: test_7(); break;
    case 8: test_8(); break;
    case 9: test_9(); break;
    default: printf("\nNo test or invalid test specified (only 1-5 and 7-9 are valid).");
    return -1;
  }
  return 0;
}
